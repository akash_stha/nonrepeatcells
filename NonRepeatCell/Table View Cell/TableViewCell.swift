//
//  TableViewCell.swift
//  NonRepeatCell
//
//  Created by Newarpunk on 2/2/20.
//  Copyright © 2020 Akash Stha. All rights reserved.
//

import UIKit

class TableViewCell : UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var onPresent: UIButton!
    @IBOutlet weak var onAbsent: UIButton!
    @IBOutlet weak var onLeave: UIButton!
    @IBOutlet weak var onLate: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
